package katarzyna.mobileapp2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AppActivity5 extends AppCompatActivity
{
    EditText name;
    EditText year;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app5);

        final Intent intent = new Intent(this, AppActivity6.class);
        Button button = (Button) findViewById(R.id.buttonPass);
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                Bundle data= new Bundle();
                name=(EditText)findViewById(R.id.editTextPassingName);
                year=(EditText)findViewById(R.id.editTextPassingDate);
                result=(TextView)findViewById(R.id.textViewResult);

                data.putString("name",name.getText().toString());
                try
                {
                    data.putInt("year", Integer.parseInt(year.getText().toString()));
                }
                catch(NumberFormatException ex)
                {
                    data.putInt("year", 0);
                }
                intent.putExtras(data);
                startActivityForResult(intent, 123);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int reqID, int resC, Intent i)
    {
        if(resC== Activity.RESULT_OK && reqID==123)
        {
            Bundle resData= new Bundle();
            resData=i.getExtras();
            int res = resData.getInt("result");
            String newText="Wiek: "+ res;
            result.setText(newText);
        }
    }


    public void goBackPress(View view)
    {
        onBackPressed();
    }
}
