package katarzyna.mobileapp2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.Toast;

public class AppActivity2 extends AppCompatActivity
{
    EditText surname;
    EditText name;
    CheckBox  university1;
    CheckBox  university2;
    RadioButton working;
    RatingBar stars;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app2);

        surname=(EditText)findViewById(R.id.editTextSurname);
        name=(EditText)findViewById(R.id.editTextName);
        university1=(CheckBox)findViewById(R.id.checkBox1);
        university2=(CheckBox)findViewById(R.id.checkBox2);
        working=(RadioButton)findViewById(R.id.radioButton);
        stars=(RatingBar)findViewById(R.id.ratingBar);
    }

    public void goBack(View view)
    {
        onBackPressed();
    }

    public void showToast(View view)
    {
        String data="";
        data+="Nazwisko: "+ surname.getText().toString() + " \nImie: "+name.getText().toString();
        if(university1.isChecked())
        {
            data+="\n"+university1.getText().toString();
        }
        if(university2.isChecked())
        {
            data+="\n"+university2.getText().toString();
        }
        if(working.isChecked())
        {
            data+="\n"+working.getText().toString();
        }
        data+="\nOcena ankiety: " + stars.getRating();
        Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
    }
}
