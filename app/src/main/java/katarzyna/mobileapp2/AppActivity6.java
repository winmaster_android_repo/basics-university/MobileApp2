package katarzyna.mobileapp2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

public class AppActivity6 extends AppCompatActivity
{
    TextView receivedName;
    TextView receivedYear;
    TextView receivedRes;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app6);

        Intent i=getIntent();
        Bundle data= new Bundle();
        data=i.getExtras();
        String name=data.getString("name");
        int year=data.getInt("year");

        Calendar calendar = Calendar.getInstance();
        int currentYear= calendar.get(Calendar.YEAR);

        int result=currentYear-year;

        //ustawianie etykiet
        receivedName=(TextView)findViewById(R.id.textViewReceivedName);
        receivedYear=(TextView)findViewById(R.id.textViewReceivedDate);
        receivedRes=(TextView)findViewById(R.id.textViewReceivedValue);

        String newTextName=receivedName.getText().toString() + " " +name;
        String newTextYear=receivedYear.getText().toString() + " " + year;
        String newTextRes=receivedRes.getText().toString()+ " " +result;

        receivedName.setText(newTextName);
        receivedYear.setText(newTextYear);
        receivedRes.setText(newTextRes);

        data.putInt("result", result);
        i.putExtras(data);
        setResult(Activity.RESULT_OK, i);
    }

    public void goBackPressed(View view)
    {
        onBackPressed();
    }
}
