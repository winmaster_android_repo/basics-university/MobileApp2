package katarzyna.mobileapp2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Toast;

public class AppActivity4 extends AppCompatActivity
{
    EditText surname;
    EditText name;
    CheckBox university1;
    CheckBox  university2;
    RadioButton working;
    RatingBar stars;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app4);

        surname=(EditText)findViewById(R.id.editTextSurnameTable);
        name=(EditText)findViewById(R.id.editTextNameTable);
        university1=(CheckBox)findViewById(R.id.checkBox1Table);
        university2=(CheckBox)findViewById(R.id.checkBox2Table);
        working=(RadioButton)findViewById(R.id.radioButtonTable);
        stars=(RatingBar)findViewById(R.id.ratingBarTable);
    }

    public void goBack(View view)
    {
        onBackPressed();
    }

    public void showToast(View view)
    {
        String data="";
        data+="Nazwisko: "+ surname.getText().toString() + " \nImie: "+name.getText().toString();
        if(university1.isChecked())
        {
            data+="\n"+university1.getText().toString();
        }
        if(university2.isChecked())
        {
            data+="\n"+university2.getText().toString();
        }
        if(working.isChecked())
        {
            data+="\n"+working.getText().toString();
        }
        data+="\nOcena ankiety: " + stars.getRating();
        Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
    }
}
